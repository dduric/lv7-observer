﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV_Observer
{
    static class Program
    {
        static void Main(string[] args) {

            SystemDataProvider systemDataProvider = new SystemDataProvider();

            Logger logger = new ConsoleLogger();

            while (true)
            {
                systemDataProvider.GetAvailableRAM();
                systemDataProvider.GetCPULoad();
                logger.Log(systemDataProvider);
                System.Threading.Thread.Sleep(1000);
            }


        }
    }
}
